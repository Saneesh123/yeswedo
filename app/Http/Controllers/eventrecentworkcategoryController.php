<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class eventrecentworkcategoryController extends Controller
{
    public function index(){
        $action = "";
        if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])){
            $action = $_REQUEST['action'];
        }
        $msg = (object) array();
        switch($action){
            case '' :
            default :
                    return view('admin/event/recent_work_category/index');
                    break;
            case 'Add':
                    return view('admin/event/recent_work_category/addedit'); 
                    break;
            case 'update':
                
            if(DB::table('event_recent_work_category')
                ->where('id', $_REQUEST['id'])
                ->update([ 'name' => $_REQUEST['txtName']])){
                
                    $msg->flag = 'RUS';  
                }
                else{
                    $msg->flag = 'RNUS';   
                }
                return view('admin/event/recent_work_category/index')->with('msg', $msg);  
                break;
            
                    break;
            case 'delete':
                    

                    if(DB::table('event_recent_work_category')->where('id', '=', $_REQUEST['id'])->delete()){
                        
                        $msg->flag = 'RDS';
                    }
                    else{
                        $msg->flag = 'RNDS';
                    }
                    return view('admin/event/recent_work_category/index')->with('msg', $msg);  
                    break;
            case 'edit':
                    $data = $this->getData($_REQUEST['id']);
                    $record = array();
                    $record['txtName'] = $data[0]->name;
                    $record['id'] = $_REQUEST['id'];

                    return view('admin/event/recent_work_category/addedit')->with('data', $record); 
                    break;
            case 'add':
                    $id = DB::table('event_recent_work_category')->insertGetId(
                        ['name' => htmlentities( $_REQUEST['txtName'], ENT_QUOTES, 'UTF-8', false),
                        'created_at'=> date('Y-m-d H:i:s')
                        ]
                    );
                    if($id > 0 && $id != '' && isset($id)){
                        $msg->flag = 'RAS';
                        
                    }
                    else{
                        $msg->flag = 'RNAS';  
                    }    
                    return view('admin/event/recent_work_category/index')->with('msg', $msg);                                                                              
                    break;
            case "changeStatus":
            
                    if($_REQUEST['active'] == 'Y'){
                        $status = 'N' ;
                    }
                    else{ 
                        $status = 'Y';
                    }
                    DB::table('event_recent_work_category')
                    ->where('id', $_REQUEST['id'])
                    ->update([ 'active' => $status]);
                    if($status == 'N'){
                        $msg->flag = 'RIS';  
                    }
                    else{
                        $msg->flag = 'RATS';   
                    }
                    return view('admin/event/recent_work_category/index')->with('msg', $msg);  
                    break;
                    
        }
    }
    //Fetch all record
    public static function getAllData(){
        //$data = DB::select('SELECT * FROM event_recent_work_category WHERE deleted_at is NULL');
        $data = DB::table('event_recent_work_category')->whereNull('deleted_at')->paginate(10);
        return $data;
    }
    // Fetch a single record
    public static function getData($id){
        $SelQry = "SELECT * FROM event_recent_work_category 
                    WHERE id = ". $id ." 
                    AND deleted_at is NULL";
        $data = DB::select($SelQry);
        return $data;
    }

    //array for flag messages

    public static function flagMessages($flagMsg){

        $arrMessages = array(

            'RAS' => "Record added successfully.",
            'RNAS' => "Record not added successfully",
            'RDS' => "Record deleted successfully",
            "RNDS" => "Record not deleted successfully",
            "RUS" => "Record updated successfully",
            'RNUS' => "Record not updated successfully",
            "RIS" => "Record inactivated successfully",
            "RATS" => "Record activated successfully",
            "RUS" => "Record updated successfully",
            "RNUS" => "Record not updated successfully"

        );

        return $arrMessages[$flagMsg];
       
    }
}
