<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class eventController extends Controller
{
    public function index(){
        $action = "";
        if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])){
            $action = $_REQUEST['action'];
        }
        $msg = (object) array();
        switch($action){
            case '' :
            default :
                    return view('admin/event/what_we_do/index');
                    break;
            case 'Add':
                    return view('admin/event/what_we_do/addedit'); 
                    break;
            case 'update':
                
            if(DB::table('event_whatwedo')
                ->where('id', $_REQUEST['id'])
                ->update([ 'title' => $_REQUEST['txttitle'], 'content' => $_REQUEST['editor'], 'icon' => $_REQUEST['txtIcon']])){
                
                    $msg->flag = 'RUS';  
                }
                else{
                    $msg->flag = 'RNUS';   
                }
                return view('admin/event/what_we_do/index')->with('msg', $msg);  
                break;
            
                    break;
            case 'delete':
                    

                    if(DB::table('event_whatwedo')->where('id', '=', $_REQUEST['id'])->delete()){
                        
                        $msg->flag = 'RDS';
                    }
                    else{
                        $msg->flag = 'RNDS';
                    }
                    return view('admin/event/what_we_do/index')->with('msg', $msg);  
                    break;
            case 'edit':
                    $data = $this->getData($_REQUEST['id']);
                    $record = array();
                    $record['txttitle'] = $data[0]->title;
                    $record['txtIcon'] = $data[0]->icon;
                    $record['editor'] = $data[0]->content;
                    $record['id'] = $_REQUEST['id'];

                    return view('admin/event/what_we_do/addedit')->with('data', $record); 
                    break;
            case 'add':
                    $id = DB::table('event_whatwedo')->insertGetId(
                        ['title' => htmlentities( $_REQUEST['txttitle'], ENT_QUOTES, 'UTF-8', false), 
                        'content' => htmlentities( $_REQUEST['editor'], ENT_QUOTES, 'UTF-8', false),
                        'icon' => htmlentities( $_REQUEST['txtIcon'], ENT_QUOTES, 'UTF-8', false),
                        'created_at'=> date('Y-m-d H:i:s')
                        ]
                    );
                    if($id > 0 && $id != '' && isset($id)){
                        $msg->flag = 'RAS';
                        
                    }
                    else{
                        $msg->flag = 'RNAS';  
                    }    
                    return view('admin/event/what_we_do/index')->with('msg', $msg);                                                                              
                    break;
            case "changeStatus":
            
                    if($_REQUEST['active'] == 'Y'){
                        $status = 'N' ;
                    }
                    else{ 
                        $status = 'Y';
                    }
                    DB::table('event_whatwedo')
                    ->where('id', $_REQUEST['id'])
                    ->update([ 'active' => $status]);
                    if($status == 'N'){
                        $msg->flag = 'RIS';  
                    }
                    else{
                        $msg->flag = 'RATS';   
                    }
                    return view('admin/event/what_we_do/index')->with('msg', $msg);  
                    break;
                    
        }
    }
    //Fetch all record
    public static function getAllData(){
        //$data = DB::select('SELECT * FROM event_whatwedo WHERE deleted_at is NULL');
        $data = DB::table('event_whatwedo')->whereNull('deleted_at')->paginate(10);
        return $data;
    }
    // Fetch a single record
    public static function getData($id){
        $SelQry = "SELECT * FROM event_whatwedo 
                    WHERE id = ". $id ." 
                    AND deleted_at is NULL";
        $data = DB::select($SelQry);
        return $data;
    }

    //array for flag messages

    public static function flagMessages($flagMsg){

        $arrMessages = array(

            'RAS' => "Record added successfully.",
            'RNAS' => "Record not added successfully",
            'RDS' => "Record deleted successfully",
            "RNDS" => "Record not deleted successfully",
            "RUS" => "Record updated successfully",
            'RNUS' => "Record not updated successfully",
            "RIS" => "Record inactivated successfully",
            "RATS" => "Record activated successfully",
            "RUS" => "Record updated successfully",
            "RNUS" => "Record not updated successfully"

        );

        return $arrMessages[$flagMsg];
       
    }
}
