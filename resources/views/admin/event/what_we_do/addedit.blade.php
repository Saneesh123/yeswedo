@extends('admin.includes.app')
@section('content')
<?php 
if(isset($data) && !empty($data)){
    $title = $data['txttitle'];
    $content = $data['editor'];
    $icon = $data['txtIcon'];
    $action = "update";
    $btnName = "UPDATE";
}
else{
    $title = '';
    $content = '';
    $icon = '';
    $action = 'add';
    $btnName = 'ADD';

}
?><div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">
            <div class="head">
                <h3>ADD RECORD</h3>
                <div class="back-btn pull-right">
                    <a href="{{action('eventController@index', ['action' => ''])}}">Back</a>
                </div>
            </div> 
        </div>
        <?php 
        if(isset($data['success']) && $data['success'] == 'N'){
        ?><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">
                <div class="" style="color:#ff0707;">
                    <span>Record not successfully added.</span>
                </div>
            </div><?php
        }
        
        
        ?><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">
            <div class="col-md-8 col-md-offset-2 ">
                <form action="{{action('eventController@index')}}" method="POST">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="txttitle">Title</label>
                            <input type="text" class="form-control" id="txttitle" name="txttitle"  placeholder="Title" value="<?php print($title); ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="txtIcon">Icon</label>
                            <input type="text" class="form-control" id="txtIcon" name="txtIcon" placeholder="Font awsome" value="<?php print($icon); ?>">
                        </div>
                    
                        <div class="form-group col-md-12">
                            <textarea name="editor" id="editor"><?php print($content); ?></textarea>
                        </div>

                    </div><?php
                    if(isset($_REQUEST['id']) && $_REQUEST['id'] != ''){
                        ?><input type="hidden" name="id" id='id' value='<?php print($_REQUEST['id']); ?>'><?php
                    }
                    ?><input type="hidden" name="action" id="action" value="<?php print($action); ?>">
                    <button type="submit" class="btn btn-primary"><?php print($btnName);?></button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection