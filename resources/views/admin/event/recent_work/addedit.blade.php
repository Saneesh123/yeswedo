@extends('admin.includes.app')
@section('content')
<?php
use \App\Http\Controllers\eventrecentworkcategoryController; 
if(isset($data) && !empty($data)){
    $name = $data['txtName'];
    $action = "update";
    $btnName = "UPDATE";
}
else{
    $name = '';
    $action = 'add';
    $btnName = 'ADD';

}
?><div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">
            <div class="head">
                <h3>ADD RECORD</h3>
                <div class="back-btn pull-right">
                    <a href="{{action('eventrecentworkcategoryController@index', ['action' => ''])}}">Back</a>
                </div>
            </div> 
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title" style="min-height: 370px;">
            <div class="col-md-8 col-md-offset-2 ">
                <form action="{{action('eventrecentworkcategoryController@index')}}" method="POST" enctype ='multipart/form-data'>
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="txttitle">Title<span class="mandatory">*</span></label>
                            <input type="text" class="form-control" id="txtName" name="txtName"  placeholder="Category name" value="<?php print($name); ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="txttitle">Category name<span class="mandatory">*</span></label>
                            <?php
                                $category = eventrecentworkcategoryController::getAllData();
                            ?><select id="categoryName" class="form-control">
                                <option value=''>Please choose category</option>
                                @foreach($category as $cat)
                                    <option value="<?php print($cat->id); ?>">{{$cat->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="txttitle">Image<span class="mandatory">*</span></label>
                            <input type="file" class="form-control" id="fileImage" name="fileImage"  placeholder="Category name" value="<?php print($name); ?>" required>
                        </div> 
                    
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary pull-left"><?php print($btnName);?></button>
                        </div>
                    </div><?php
                    if(isset($_REQUEST['id']) && $_REQUEST['id'] != ''){
                        ?><input type="hidden" name="id" id='id' value='<?php print($_REQUEST['id']); ?>'><?php
                    }
                    ?><input type="hidden" name="action" id="action" value="<?php print($action); ?>">
                    
                </form>
            </div>
        </div>
    </div>
</div>
@endsection