<?php
    use \App\Http\Controllers\eventrecentworkController;
?>
@extends('admin.includes.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">
            <div class="head">
                <h3>Recent Works</h3>
                <div class="back-btn pull-right">
                <a class="btn btn-primary " href="{{action('eventrecentworkController@index', ['action' => 'Add'])}}">Add</a>
                <a class="btn btn-primary " href="/dashboard-events">Back</a>
                </div>
            </div> 
        </div>
        @if(isset($msg) && $msg->flag != '')
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">
                <div class="" style="color:#0b9202;">
                    <span><?php echo  eventrecentworkController::flagMessages($msg->flag);?></span>
                </div>
            </div>
            </div>
        @endif
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">
        <?php
        $data = eventrecentworkController::getAllData();
        ?>
        @if(count($data) >= 1)
            <table class="table table-hover" width="80%">
                <thead class="thead-light">
                    <tr>
                        <th>
                            name
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>   
                </thead>
                <tbody> 
                    @foreach($data as $record)
                        <tr>
                            <td>{{$record->name}}</td>
                            <td>
                            @if($record->active == 'Y')
                                <a href="{{action('eventrecentworkController@index', ['action' => 'changeStatus','id' =>$record->id,'active' => 'Y'])}}">Active</a>
                            @else  
                                <a href="{{action('eventrecentworkController@index', ['action' => 'changeStatus','id' =>$record->id,'active' => 'N'])}}">Inactive</a>
                            @endif
                            </td>
                            <td>    
                                <span>
                                    <a href="{{action('eventrecentworkController@index', ['action' => 'edit', 'id' =>$record->id])}}">Edit</a>
                                </span>
                                <span>
                                    <a href="{{action('eventrecentworkController@index', ['action' => 'delete', 'id' =>$record->id])}}">Delete</a>
                                </span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $data->links() }}
        @else
            <p>No data  </p>
        @endif 
        </div>
    </div>
</div>
@endsection